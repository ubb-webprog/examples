// Példa egy egyszerű web-szerverre amely GET kéréseket fogad és kiírja a
// console-ra a kérés metódusát, az URL-t valamint a kérés fejlécét
// Előtte telepíteni szükséges az express modult: npm install

import express from 'express';

// Elkészítjük az express alkalmazást
const app = express();

// GET hívások a "/hello" útvonalra itt landolnak
app.get('/hello', (req, res) => {
  console.log('Received a new request');
  res.send('And hello to you too.\n');
});

// HTTP GET kérésre való reakció
app.get('/*', (request, response) => {
  const info = `You have requested:
    - method: ${request.method}
    - URL: ${request.url}
    - headers (as JSON):
      ${JSON.stringify(request.headers)}
  `;
  console.log(info);
  response.send(info);
});

// A listen metódus hívásának hatására aktiválódik a szerver
// és fogadja a 8080-as porton beérkező kéréseket
app.listen(8080, () => {
  console.log('Server listening on http://localhost:8080/ ...');
});
