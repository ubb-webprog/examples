import express from 'express';
import fs from 'fs';
import path from 'path';

const app = express();

const rootDir = path.join(process.cwd(), 'static');

app.get('/static/:filename', (req, res) => {
  // query-ből vesszük a file-t, de NEM DOLGOZZUK FEL
  // lehet kilépni a folderből ".."-tal, ha urlencoded-del védjük a "/" karaktert
  // példa:
  //   GET http://localhost:8080/static/..%2Findex.js
  const filename = path.join(rootDir, req.params.filename);
  if (fs.existsSync(filename)) {
    res.sendFile(filename);
  } else {
    res.status(404).send('File does not exist');
  }
});

app.listen(8080, () => {
  console.log('Server listening on http://localhost:8080/ ...');
});
