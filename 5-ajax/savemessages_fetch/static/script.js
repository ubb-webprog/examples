async function getMessages() {
  const response = await fetch('/messages');
  const messages = await response.json();
  document.getElementById('messages').value = JSON.stringify(messages, 0, 2);
}

async function sendMessage() {
  await fetch('/messages', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      text: document.getElementById('newMessage').value,
      date: new Date(),
    }),
  });
  await getMessages();
}

window.onload = () => {
  document.getElementById('sendMessage').addEventListener('click', sendMessage);
  getMessages();
};
