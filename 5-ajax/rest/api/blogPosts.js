import express from 'express';
import * as blogPostDao from '../db/blogPosts.js';
import * as userDao from '../db/users.js';
import hasProps from '../middleware/validate.js';

const router = new express.Router();

// findAll
router.get('/', async (req, res) => {
  try {
    const blogPosts = await blogPostDao.findAllBlogPosts();
    res.json(blogPosts);
  } catch (err) {
    res.status(500).json({ message: `Error while finding all blog posts: ${err.message}` });
  }
});

// findById
router.get('/:blogPostId', async (req, res) => {
  const { blogPostId } = req.params;

  try {
    const blogPost = await blogPostDao.findBlogPostById(blogPostId);
    if (!blogPost) {
      res.status(404).json({ message: `Blog post with ID ${blogPostId} not found.` });
      return;
    }
    res.json(blogPost);
  } catch (err) {
    res.status(500).json({ message: `Error while finding blog post with ID ${blogPostId}: ${err.message}` });
  }
});

// findById => author
router.get('/:blogPostId/author', async (req, res) => {
  const { blogPostId } = req.params;
  try {
    const user = await userDao.findAuthorByBlogPostId(blogPostId);
    if (!user) {
      res.status(404).json({ message: `Author of blog post with ID ${blogPostId} not found.` });
      return;
    }
    res.json(user);
  } catch (err) {
    res.status(500).json({ message: `Error while finding author for blog post with ID ${blogPostId}: ${err.message}` });
  }
});

// insert
router.post('/', hasProps(['title', 'content', 'authorId']), async (req, res) => {
  try {
    const exists = await userDao.userExists(req.body.authorId);
    if (!exists) {
      res.status(400).json({ message: `User with ID ${req.body.authorId} does not exist` });
      return;
    }

    const blogPost = await blogPostDao.insertBlogPost(req.body);
    res.status(201).location(`${req.fullUrl}/${blogPost.id}`).json(blogPost);
  } catch (err) {
    res.status(500).json({ message: `Error while creating blog post: ${err.message}` });
  }
});

// delete
router.delete('/:blogPostId', async (req, res) => {
  const { blogPostId } = req.params;
  try {
    const exists = await blogPostDao.deleteBlogPost(blogPostId);
    if (!exists) {
      res.status(404).json({ message: `BlogPost with ID ${blogPostId} not found.` });
      return;
    }
    res.sendStatus(204);
  } catch (err) {
    res.status(500).json({ message: `Error while deleting blog post with ID ${blogPostId}: ${err.message}` });
  }
});

// update put
router.put('/:blogPostId', hasProps(['title', 'content', 'authorId']), async (req, res) => {
  const { blogPostId } = req.params;
  try {
    await blogPostDao.updateOrInsertBlogPost(blogPostId, req.body);
    res.sendStatus(204);
  } catch (err) {
    res.status(500).json({ message: `Error while updating blog post with ID ${blogPostId}: ${err.message}` });
  }
});

// update patch
router.patch('/:blogPostId', async (req, res) => {
  const { blogPostId } = req.params;
  try {
    const exists = await blogPostDao.updateBlogPost(blogPostId, req.body);
    if (!exists) {
      res.status(404).json({ message: `BlogPost with ID ${blogPostId} not found.` });
      return;
    }
    res.sendStatus(204);
  } catch (err) {
    res.status(500).json({ message: `Error while updating blog post with ID ${blogPostId}: ${err.message}` });
  }
});

export default router;
