import url from 'url';

// segédfüggvény: minden hívásnak beállítja a teljes URL-jét
// segít a HATEOAS kialakításában
export default (req, res, next) => {
  req.fullUrl = url.format({
    protocol: req.protocol,
    host: req.get('host'),
    pathname: req.originalUrl,
  });

  next();
};
