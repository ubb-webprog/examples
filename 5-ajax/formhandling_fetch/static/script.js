async function submitForm(event) {
  // az esemény eredeti lekezelését (szinkron leadás) fölülírjuk
  event.preventDefault();

  // az esemény a formra van értelmezve
  const form = event.target;
  // kivesszük a form adatait
  const formData = new FormData(form);

  // aszinkron kérést küldünk
  const response = await fetch('/submit_form', {
    method: 'POST',
    body: formData,
  });
  const responseText = await response.text();
  document.getElementById('result').value = responseText;
}

window.onload = () => {
  document.getElementById('myForm').addEventListener('submit', submitForm);
};
