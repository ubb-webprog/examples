/*
Ez a példa egyirányú hashelést végez jelszavak titkosítására.
*/
import crypto from 'crypto';
import express from 'express';

const saltSize = 30;

const app = express();

// generálunk hash-t egy jelszóból, de nem sózunk (NEM AJÁNLOTT)
app.post('/create_hash_nosalt', express.urlencoded({ extended: true }), (req, res) => {
  const { password } = req.body;
  // hash készítése
  const hash = crypto.createHash('sha512').update(password).digest('base64');
  // a hash-t tárolhatnánk adatbázisban
  res.send(hash);
});

// ellenőrizzük egy megadott jelszóról hogy megfelel-e
// egy megadott hashnek (NINCS SÓ, NEM AJÁNLOTT)
app.post('/check_hash_nosalt', express.urlencoded({ extended: true }), (req, res) => {
  // a konkatenált hash-t és sót adatbázisból kérnénk le
  const { password, hash } = req.body;
  // base64 string dekódolás
  const expectedHash = Buffer.from(hash, 'base64');
  // újra-hash-elés
  const actualHash = crypto.createHash('sha512').update(password).digest();

  if (expectedHash.equals(actualHash)) {
    res.send('Passwords match');
  } else {
    res.status(401).send('Passwords do not match');
  }
});

// generálunk hash-t egy jelszóból
app.post('/create_hash', express.urlencoded({ extended: true }), (req, res) => {
  const { password } = req.body;
  // só generálása
  const salt = crypto.randomBytes(saltSize);
  // hash készítése
  const hash = crypto.createHash('sha512').update(password).update(salt).digest();
  // konkatenálás és base64 stringgé alakítás
  const hashWithSalt = `${hash.toString('base64')}:${salt.toString('base64')}`;
  // a konkatenált hash-t és sót tárolnánk adatbázisban
  res.send(hashWithSalt);
});

// ellenőrizzük egy megadott jelszóról hogy megfelel-e
// egy megadott hashnek
app.post('/check_hash', express.urlencoded({ extended: true }), (req, res) => {
  // a konkatenált hash-t és sót adatbázisból kérnénk le
  const { password, hashWithSalt } = req.body;
  // base64 string dekódolás és dekonkatenálás
  const [expectedHashB64, saltB64] = hashWithSalt.split(':');
  const expectedHash = Buffer.from(expectedHashB64, 'base64');
  const salt = Buffer.from(saltB64, 'base64');
  // újra-hash-elés
  const actualHash = crypto.createHash('sha512').update(password).update(salt).digest();

  if (expectedHash.equals(actualHash)) {
    res.send('Passwords match');
  } else {
    res.status(401).send('Passwords do not match');
  }
});

app.listen(8080, () => {
  console.log('Server listening on http://localhost:8080/ ...');
});
