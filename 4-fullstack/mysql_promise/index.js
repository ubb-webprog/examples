/*
Csatlakozik egy MySQL adatbázishoz egy connection pool segítségével.
Minden HTTP kérés érkezése esetén beírja az adatbázisba
a kérés metódusát, az URL-t és a kérés időpontját.

Figyelem! npm i csak a MySQL-hez csatlakoztató segédmodult telepíti, nem magát a MySQL-t.
*/
import express from 'express';
import mysql from 'mysql2/promise.js';

// Létrehozunk egy connection poolt
// a megadott DB-nek/felhasználónak léteznie kell
// létre lehet hozni a mellékelt setup.sql szkript segítségével
const pool = mysql.createPool({
  connectionLimit: 10,
  database: 'webprog',
  host: 'localhost',
  port: 3306,
  user: 'webprog',
  password: 'VgJUjBd8',
});

// létrehozzunk a táblázatot, ha még nem létezik
// a pool.query kér egy kapcsolatot a poolból
await pool.query(
  `CREATE TABLE IF NOT EXISTS requests (
    method varchar(20),
    url varchar(50),
    date datetime);`,
);
console.log('Table exists successfully');

const app = express();

// DB-tartalom visszatérítése
app.get('/requests', async (req, res) => {
  try {
    const result = await pool.query('SELECT * FROM requests');
    res.send(result[0]);
  } catch (err) {
    const msg = `Selection unsuccessful: ${err}`;
    res.status(500).send(msg);
  }
});

app.use(async (req, res) => {
  try {
    const queryString = 'INSERT INTO requests VALUES (?, ?, ?)';
    const values = [req.method, req.url, new Date()];

    console.log(`Executing query '${queryString}' with values: [${values}]`);
    const result = await pool.query(queryString, values);
    res.send(`Successfully inserted ${result[0].affectedRows} rows`);
  } catch (err) {
    const msg = `Insertion unsuccessful: ${err}`;
    res.status(500).send(msg);
  }
});

app.listen(8080, () => {
  console.log('Server listening on http://localhost:8080/ ...');
});
