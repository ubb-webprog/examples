# Callback hell
Az alábbi példaprogramok az úgynevezett callback hell problémára kinálnak megoldásokat:
- `1_callback_hell_problem` katalógusban magát a problémát ismertetjük;
- `2_callback_hell_function_handler` - function handlerek használata segítségével szétbontjuk kisebb részekre az alkalmazást;
- `3 callback_hell_promise` - egyszerű promise példa, valamint a feladat helyes megoldása promise segítségével;
- `4_callback_hell_async_await` - a feladat megoldása async/await segítségével.

## A feladat
A példaprogram előadók, az általuk kiadott albumok valamint az albumokon található zeneszámok adatbázisát kezeli. A feladat az, hogy listázzuk hierarchiában egy előadóhoz tartozó összes albumot, valamint az albumokon levő összes zeneszámot.

FIGYELEM: Ezt a feladatot egyetlen jól megírt SQL lekérdezéssel is meg lehetne oldani, viszont a probléma szemléltetése és a feladat egyszerűsége véget ezt három külön lekérdezésben fogjuk megtenni. Első lépésben az előadóhoz tartozó információkat fogjuk lekérdezni, majd második lépésben az előadó információi segítségével lekérdezzük a hozzá tartozó albumokat majd utolsó lépésként az albumokhoz tartozó zeneszámokat.

Az elvárt kiíratás a következőképpen kell kinézzen:
- előadó neve
  - előadóhoz tartozó albumok
    - az albumokon levő zeneszámok

FIGYELEM: az első két megoldás nem teljesíti ez utóbbi követelményt mivel az aszinkron hívások nem várják be egymást!

## Telepítés
Előfeltétel:
- telepített MySql vagy MariaDb;
- NodeJS 10+;
- Npm vagy Yarn csomagkezelő.

Lépések a telepítéshez:
- importáljuk be a music.sql állományt az adatbázisunkba;
- futtasuk a `yarn install` vagy `npm install` parancsot;
- `yarn start` vagy `npm start` a program indításához.