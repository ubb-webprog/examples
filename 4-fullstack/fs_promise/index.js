// fs Promise-okat használó változata
import fs from 'fs/promises';

//
// fájlolvasás Promise-szal
//
fs.readFile('example')
  .then((content) => {
    console.log('Ex 1 read file with promise');
    console.log(content.toString());
  })
  .catch((err) => console.error(`Error reading file: ${err}`));

//
// await írásmódja ugyanannak
//
const content = await fs.readFile('example');
console.log('Ex 2 read file with promise+await');
console.log(content.toString());

//
// Promise láncolás - olvasás + nagybetűsítés + írás
//
function capitalize(fn) {
  return fs
    .readFile(fn)
    .then((readContent) => readContent.toString().toUpperCase())
    .then((upperCaseContent) => fs.writeFile(`${fn}_upper`, upperCaseContent));
}

capitalize('example')
  .then(() => console.log('Ex 3 transformed file with promise chain'))
  .catch((err) => console.error(`Error transforming file: ${err}`));

//
// async/await írásmódja ugyanannak
//
async function lowerize(fn) {
  const readContent = await fs.readFile(fn);
  const upperCaseContent = readContent.toString().toLowerCase();
  await fs.writeFile(`${fn}_lower`, upperCaseContent);
}

await lowerize('example');
console.log('Ex 4 transformed file with async/await');
