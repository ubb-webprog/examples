const rectW = 55;
const imgW = 50;

function clearCanvas() {
  const canvas = document.getElementById('canvas');
  const context = canvas.getContext('2d');
  context.clearRect(0, 0, canvas.width, canvas.height);
}

window.onload = () => {
  document.getElementById('clearButton').addEventListener('click', clearCanvas);
  console.log('Loading...');
  const canvas = document.getElementById('canvas');

  // rendelünk egy figyelőt a canvashez
  canvas.addEventListener('click', (event) => {
    const x = event.pageX - canvas.offsetLeft;
    const y = event.pageY - canvas.offsetTop;
    console.log(`x: ${x},  y: ${y}`);

    const context = canvas.getContext('2d');
    // rajzolunk egy kockát a klikk poziciója köré
    context.fillRect(x - rectW / 2, y - rectW / 2, rectW, rectW);
    // majd arra az ábrát
    context.drawImage(document.getElementById('image'), x - imgW / 2, y - imgW / 2, imgW, imgW);
  });
};
