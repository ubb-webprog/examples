// objektumkonstruktor
function MyObject(name, size) {
  this.name = name;
  this.size = size;
  this.tellSize = (elementId) => {
    document.getElementById(elementId).childNodes[0].nodeValue = `A ${this.name} mérete ${this.size}${this.unit}`;
  };
}

window.onload = () => {
  // utólagos prototípus kiterjesztés
  MyObject.prototype.unit = 'm';

  const myobj = new MyObject('nadrágszíj', 5);
  document.getElementById('button').addEventListener('click', myobj.tellSize('target'));
};
