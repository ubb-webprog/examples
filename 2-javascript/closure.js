const fib = (() => {
  let f1 = 0;
  let f2 = 1;
  return () => {
    const x = f1;
    f1 = f2;
    f2 += x;
    return f2;
  };
})();

function myFunction() {
  document.getElementById('demo').childNodes[0].nodeValue = fib();
}

window.onload = () => {
  document.getElementById('fibButton').addEventListener('click', myFunction);
};
