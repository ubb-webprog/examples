const object = [];
const array = ['szilva', 'körte', 'cseresznye'];

function iterate1() {
  // Végigiterálva a tömbbön
  array.forEach((value, index) => {
    console.log(`kulcs: ${index}, érték: ${value}`);
  });
}

function iterate2() {
  // Végigiterálva az objektumon
  Object.keys(object).forEach(([key, value]) => {
    console.log(`kulcs: ${key}, érték: ${value}`);
  });
}

function doPrompt() {
  const userNumber = Number(prompt('Kérlek adj meg egy számot'));
  switch (userNumber) {
    case 1:
      console.log('Egyest adtál meg');
      break;
    case 2:
      console.log('Kettest adtál meg');
      break;
    default:
      console.log('Nem tudtam értelmezni a számot');
      break;
  }
}

window.onload = () => {
  object['gyümölcs'] = 'körte';
  object['zöldség'] = 'krumpli/pityóka';

  document.getElementById('prompt').addEventListener('click', doPrompt);
  document.getElementById('iter').addEventListener('click', iterate1);
  document.getElementById('iter2').addEventListener('click', iterate2);
};
