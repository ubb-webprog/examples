// belső (relatív útvonalon elhelyezkedő)
// modul betöltése
// eslint-disable-next-line import/no-unresolved
import mustache from 'https://cdnjs.cloudflare.com/ajax/libs/mustache.js/4.2.0/mustache.min.js';
import libFunction from './es2015module_default.js';
import { libFunction1 } from './es2015module_multi.js';

// az összes exportált függvény
// közös néven való betöltése
import * as libMulti from './es2015module_multi.js';

// külső modul betöltése URL-lel

// függvények használata
libFunction();
libFunction1();
libMulti.libFunction2();
console.log(mustache.escape('<b>hello</b>'));
