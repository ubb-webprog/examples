const examples = [
  { href: 'object.html', title: 'Egyszerű objektum' },
  { href: 'closure.html', title: 'Fibonacci számolás (closure)' },
  { href: 'javascript-examples.html', title: 'Objektumok / Beolvasás' },
  { href: 'iterating-over-fields.html', title: 'Objektumok iterálása' },
  { href: 'object-constructor.html', title: 'Objektum konstruktor' },
  { href: 'json.html', title: 'JSON' },
  { href: 'right-click.html', title: 'Jobbklikk lekezelése' },
  { href: 'exchange.html', title: 'Szövegmódosítás' },
  { href: 'information.html', title: 'Böngésző információ' },
  { href: 'event-handling.html', title: 'Eseménykezelés' },
  { href: 'append-element.html', title: 'Elemek DOM-ba adása' },
  { href: 'canvas.html', title: 'Canvas' },
];

const addElem = (parent, tagName, attributes) => {
  const node = document.createElement(tagName);
  Object.entries(attributes).forEach(([key, value]) => {
    node.setAttribute(key, value);
  });
  parent.appendChild(node);
  return node;
};

const addTextElem = (parent, text) => {
  const node = document.createTextNode(text);
  parent.appendChild(node);
};

window.onload = () => {
  console.log('Loading...');
  const rootNode = document.getElementById('container');

  examples.forEach((example) => {
    const elementNode = addElem(rootNode, 'div', { class: 'container-element' });
    const linkNode = addElem(elementNode, 'a', example);
    addTextElem(linkNode, example.title);
  });
};
