function makeObjectTable(name, object) {
  document.writeln(`
    <h2>${name}</h2>
    <table border=1>
      <tr>
        <th>Field</th>
        <th>Value</th>
      </tr>
  `);
  Object.entries(object).forEach(([fieldKey, fieldValue]) => {
    document.writeln(`
      <tr>
        <td>${fieldKey}</td>
        <td>${fieldValue}</td>
      </tr>
    `);
  });
  document.writeln('</table>');
}

window.onload = () => {
  makeObjectTable('Próba', { car: 'BMW', fruit: 'apple', animal: 'dog' });
};
